var Ajv = require('ajv');
const {ratingSchema,studentSchema,personSchema} = require('./schemas')
const {ratingTestCases,studentTestCases,personTestCases} = require('./testCases');

let ajv = new Ajv();

let validateRating = ajv.compile(ratingSchema);

console.log("\nTest cases for rating schema :\n")

ratingTestCases.forEach((testCase,index) => {
    let valid = validateRating(testCase);
    if (valid){
        console.log(`Test case ${index+1} passed`);
    }
    else{
        console.log(`Test case ${index+1} failed`);
    }
});

let validateStudent = ajv.compile(studentSchema);

console.log("\n\nTest cases for Student schema :\n")

studentTestCases.forEach((testCase,index) => {
    let valid = validateStudent(testCase);
    if (valid){
        console.log(`Test case ${index+1} passed`);
    }
    else{
        console.log(`Test case ${index+1} failed`);
    }
});

let validatePerson = ajv.compile(personSchema);

console.log("\n\nTest cases for Person schema :\n")

personTestCases.forEach((testCase,index) => {
    let valid = validatePerson(testCase);
    if (valid){
        console.log(`Test case ${index+1} passed`);
    }
    else{
        console.log(`Test case ${index+1} failed`);
    }
});