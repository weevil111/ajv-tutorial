const ratingTestCases = [
    // INVALID CASES :
    {
        value: "4",
    },
    {
        value: false,
        total: 85
    },
    {
        value: 6,
        total: 0
    },
    {
        value: 2,
        total: 65,
        extraField: 5
    },
    {
        value: 5,
        total: 0 // if value is present then total cannot be 0
    },
    // VALID CASES :
    {
        // No values
    },
    {
        value: 1,
        total: 5
    },
]

const studentTestCases = [
    // INVALID CASES
    {

    },
    {
        roll : 1703310008
    },
    {
        name : "Abhinav",
        roll : 1703310008,
        batch : "Invalid batch",
        year : 4 
    },
    {
        name : "Abhinav",
        roll : 1703310008,
        batch: 2021,
        branch: "Invalid branch",
        year: 4
    },
    {
        name : "Abhinav",
        roll : 1703310008,
        branch : 2021
    },
    // VALID cases
    {
        name : "Abhinav",
        roll : 1703310008,
        batch: 2021,
        branch: "CSE",
        year: 4
    },
    {
        name : "apm",
        roll : 1703310518,
        batch: 2022,
        branch: "ME",
        year: 3
    }
]

const personTestCases = [
    // INVALID CASES
    {

    },
    {
        "name": {
            "lastName": "Abhinav"
        }
    },
    {
        "name": {
            "firstName" : "Abhinav",
            "lastName" : "Mishra" 
        }
    },
    {
        "name": {
            "firstName" : "Abhinav",
            "lastName" : "Mishra" 
        },
        "gender" : "invalid gender"
    },
    {
        "name": {
            "firstName" : "Abhinav",
            "lastName" : "Mishra" 
        },
        "gender" : "male",
        "dob" : "5 March"
    },
    // VALID CASES
    {
        "name": {
            "firstName" : "Abhinav",
            "lastName" : "Mishra" 
        },
        "gender" : "male",
        "dob" : "2020-11-15"
    },
    {
        "name": {
            "firstName" : "Abhinav",
            "middleName" : "Parag",
            "lastName" : "Mishra" 
        },
        "gender" : "male",
        "dob" : "2020-11-15"
    }
]

module.exports = {ratingTestCases,studentTestCases,personTestCases};