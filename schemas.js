const ratingSchema = {
    "title": "Rating schema",
    "type": "object",
    "properties": {
        "value": {
            "type": "number",
            "minimum": 0,
            "maximum": 5
        },
        "total": {
            "type": "number",
            "minimum": 0
        }
    },
    "dependencies":{
        "value":{
            "properties":{
                "total": {
                    "exclusiveMinimum": 0 // if value is present then total cannot be 0
                }
            }
        }
    },
    "additionalProperties" : false
}

const studentSchema = {
    "title" : "Student schema",
    "type" : "object",
    "properties": {
        "name": {"type": "string"},
        "roll" : {
            "type": "number",
            "minimum": 1700000000,
            "maximum": 2099999999
        },
        "batch": {
            "type": "number",
            "enum": [2021,2022,2023,2024]
        },
        "branch": {
            "type" : "string",
            "enum" : ["CSE", "ECE", "EEE", "ME", "CE", "IT"]
        },
        "year": {
            "type" : "number",
            "enum" : [1,2,3,4]
        }
    },
    "required" : ["roll","name","year"]
}

const personSchema = {
    "type": "object",
    "properties": {
        "name": {
            "type": "object",
            "properties": {
                "firstName": {"type": "string"},
                "middleName": {"type": "string"},
                "lastName": {"type": "string"}
            },
            "required": ["firstName"],
            "additionalProperties" : false
        },
        "gender": {
            "type" : "string",
            "enum" : ["male","female","other"]
        },
        "dob": {
            "type": "string",
            "format": "date"
        }
    },
    "required" : ["name","gender"]
}

module.exports = {ratingSchema,studentSchema,personSchema}